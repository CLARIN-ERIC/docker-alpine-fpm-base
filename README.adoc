= CLARIN PHP 8 FPM base image
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

Docker base image for creating PHP 8 FPM servers.
Processes are managed by supervisord and log aggregation is handled with fluentd (inherited from https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base[alpine base image]).

The image can be used either by extending it or supplying host mounts for PHP and FPM server configuration.

== Installed PHP modules:

* session
* json
* mysqli
* pdo
* pdo_mysql
* zip
* xmlreader
* mbstring
* gd
* opcache
* ldap
* dev
* curl
* ctype
* simplexml
* zlib
* iconv
* apcu

== Configuration

=== Inherited configuration

Configuration inherited from the https://gitlab.com/CLARIN-ERIC/docker-alpine-supervisor-base[alpine base image]:

=== Configuring PHP and FPM
Supply extra FPM pool configuration files via a docker volume at /etc/php8/php-fpm.d/. Do this either in your compose configuration or in the dockerfile which extends this image.
If no extra FPM pool configurations are supplied the server will run the default *[www]* pool as *${FPM_USER}* user and group

Supply extra PHP 8 .ini configuration files via a docker volume at /etc/php8/conf.d/. Do this either in your compose configuration or in the dockerfile which extends this image.

